# SecuritEaseAutomationSolution



## Getting started

# The project itself has packages under packages.config
-You dont have to install them from scratch, but just Right Click on a solution and Restore Nugget Packages and Visual studio will do everything for you/

#Unless the Packages.Config does no have pacakges then The following nugget packages will be required to execute tests:



Install-Package Specflow
Install-Package SpecFlow.NUnit
Install-Package NUnit
Install-Package NUnit.Console
Install-Package Selenium.WebDriver
Install-Package Selenium.Chrome.WebDriver
Install-Package  Microsoft.NET.Test.Sdk
Install-Package selenium.support
Install-Package SpecFlow.Tools.MsBuild.Generation
Install-Package NUnit3TestAdapter
Install-Package RestSharp
Install-Package  ExtentReport.Core
Install-Package Newtonsoft.Json;
Also Install Specflow For Visual Studio under Extensions in visual Studio.

Make sure you install Chrome browser version 122 on your local machine, as the latest version of Selenium.Chrome.WebDriver package is 122, Other than that you can't execute selenium tests
How to clone the project from GitLab:

You can clone this proejct using the link - https://gitlab.com/omuhle.mazibuko1/securiteaseautomationsolution.git

IDE:
Visual Studio 2022 Community version
C#. Net
Framework Used:
API - RestSharp  used
Web - Selenium used
BDD - Cucumber for .Net(Specflow) used
Unit Test Framework - NUnit
How to Execute Tests:
-Navigate to Test menu on visual Sudio >> Click test Explorer

Then Build the project >> 5 Tests should appear on Test Explorer as per number of scenarios inside feature files.
Right Click on each test and Click Run.

Solution Overview:

This is Behaviour Driven Development Framework 
Test Data is stored on the feature files.
Configs (web urls,api endpoints) are NOT hard-coded they are stored on the App.Config file.
We having WebCore and ApiCore Classes they posses reusable functions for Selenium and RestSharp.
There is a JsonSchema that I created inside the default project folder from Converting the Json Response and the schema is used to be validated against JsonResponse.
