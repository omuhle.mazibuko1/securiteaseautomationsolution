﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SecuritEaseAssesment.Functions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritEaseAssesment.Core
{
    internal class WebCore
    {
        public static IWebDriver driver;


        //Reusable Selenium Funtion: To instantiate driver and open Chrome Window 
        public static void initDriver()
        {
            string url = ConfigurationManager.AppSettings["MarketWatchUrl"];
            driver = new ChromeDriver();
            driver.Url = url;
            waitForPageLoad();
            stopPageLoad();
            driver.Manage().Window.Maximize();
        }


        //Reusable Selenium Funtion: To find any element using any locator 
        public static IWebElement getElement(By xpath)
        {
            IWebElement foundElement = driver.FindElement(xpath);
            return foundElement;
        }

        //Reusable Selenium Funtion: To find multiple elements using single locator and Print Text inside #Number Of Required Elements 

        public static void getMultipleElementsThenExtractText(By xpath,int RequiredElementsCount)
        {
            try
            {
                IReadOnlyList<IWebElement> multipleElements = driver.FindElements(xpath);
                Assert.IsNotNull(multipleElements);

                for (int k = 0; k <= RequiredElementsCount; k++)
                {
                    Assert.IsNotEmpty(multipleElements.ElementAt(k).Text);
                    Console.WriteLine(multipleElements.ElementAt(k).Text);
                }
            }
            catch (StaleElementReferenceException ex)
            { 
              ex.ToString();
            }
        }


        //Reusable Selenium Funtion: To find multiple elements using single locator and Put them in a list and return the List of Elements
        [Obsolete]
        public static List<IWebElement> getListOfElementsThenExtractText(By xpath)
        {  
            IReadOnlyList<IWebElement> multipleElements = driver.FindElements(xpath);
            Assert.IsNotNull(multipleElements);

            try
            {
                for (int k = 0; k <= multipleElements.Count; k++)
                {
                    Assert.IsNotEmpty(multipleElements.ElementAt(k).Text);
                    Console.WriteLine(multipleElements.ElementAt(k).Text);
                }
            }
            catch (StaleElementReferenceException ex)
            {
                ex.ToString();
            }

            return (List<IWebElement>)multipleElements;
        }

        //Reusable Selenium Funtion: To find the element inside ShadowRoot and Click it. So as the "MarketWatch Tab"
        public static void getElementInsideShadowRoot(string shadowRootHostTag,By shadowRootInsideElementSelector,By targetElementXpath)
        {
            try
            {
                IWebElement shadowHost = driver.FindElement(By.TagName(shadowRootHostTag));
                ISearchContext shadowRoot = shadowHost.GetShadowRoot();
                IWebElement shadowContent = shadowRoot.FindElement(shadowRootInsideElementSelector);
                IWebElement actualElement = shadowContent.FindElement(targetElementXpath);
                actualElement.Click();
                waitForPageLoad();
                stopPageLoad();
            }
            catch (NoSuchShadowRootException ex)
            {
                ex.ToString();
            }
        }

        //Reusable Selenium Funtion: To stop the PageLoad as this application takes longer to load.
        public static void stopPageLoad()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("return window.stop");
        }

        //Reusable Selenium Funtion: To wait for the page to load
        public static void waitForPageLoad()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(12000);       
        }
    }
}
