﻿Feature: ApiTests

Countries API Tests

@tag1
Scenario: Validate the schema of the response
	Given I perform Get Request for All Countries
	When I see response is displayed
	Then I validate the response schema


	@tag1
Scenario: Count the number of countries listed in this response
	Given I perform Get Request for All Countries
	When I see response is displayed
	#Then I should count number of countries from a response >> This step has been embeded on the Json Schema validation above step


	@tag1
Scenario Outline: Locate “South Africa” and identify the official languages 
	Given I perform Get Request for All Countries
	When I see response is displayed
	Then I locate the "<country>"
	And I assert the "<language>"
	

	Examples:
	| country              | language              |
	| South Africa         | Sign Language         |