﻿Feature: WebTests

Market Watch Tests

@tag1
Scenario: Retrieve articles from the “Market Watch” tab
	Given I open MarketWatch website
	When I go to MarketWatch tab
	Then I should see and retreive first seven articles and printout the name of each article

	@tag1
Scenario: Retrieve the top 5 “Bottom Performers”
	Given I open MarketWatch website
	When I list the top five worst performing stocks indexed by the change percentage (CHG %)
	Then I print the names, last price (LAST) and the change percentage (CHG %)