﻿using SecuritEaseAssesment.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritEaseAssesment.Functions
{
    internal class ApiFunctions
    {
        public void doPerformAllCountriesGetRequest(string endPointUrl,string baseUrl)
        {
            endPointUrl = ConfigurationManager.AppSettings["CountriesApiEndPointUrl"].ToString();
            baseUrl = ConfigurationManager.AppSettings["CountriesBaseApiUrl"].ToString();
            ApiCore.requestApi(endPointUrl,baseUrl);

        }
        public void assertResponseContainsContry(string country)
        {
            ApiCore.verifyResponseContains(country);
        }

        public void assertResponseContainsLanguage(string language)
        {
            ApiCore.verifyResponseContains(language);
        }
    }
}
