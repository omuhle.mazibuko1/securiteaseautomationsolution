﻿using OpenQA.Selenium;
using SecuritEaseAssesment.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritEaseAssesment.Functions
{
    internal class WebFunctions
    {
        //Functions Below for WebFunctions:
        public void navigateMarketchTab()
        {
            WebCore.waitForPageLoad();
            WebCore.getElementInsideShadowRoot("nav-hat", shadowRootContentElement(),marketWatchTab());//market watch tab inside the Shadow Root
        }

        //assert 7 articles
        public void assertArticles()
        {
            WebCore.waitForPageLoad();
            WebCore.getMultipleElementsThenExtractText(articles(),7);
        }

        [Obsolete]

        //Function to assert and Top 5 List of of Worst Performing  Stocks Per CHG Percentage and Print to them Console. 
        public void assertListOfTopFiveWorstPerformingStocks()
        {
            List<IWebElement> lists = WebCore.getListOfElementsThenExtractText(WorstPerformingStocksPerCHGpercentage());
            string text = string.Empty;

            try
            {
                for (int k = 0; k <= 5; k++)
                {
                    lists.Sort();
                    IWebElement element = lists.AsQueryable().Min();
                    Console.WriteLine(element.Text);
                }
            }
            catch (InvalidOperationException ex)
            {
                ex.ToString();
            }
            
        }


        //Elements Below for WebFunctions: Smart Locators below with constucted Xpaths
        public By marketWatchTab()
        {
            By marketWatch = By.Id("mw-link");
            return marketWatch;
        }

        public By shadowRootContentElement()
        {
            By contentEle = By.ClassName("items-wrapper");
            return contentEle;
        }

        //THis is generic article Xpath for articles
        public By articles()
        {
            By article = By.XPath("//span[@class='headline']");
            return article;
        }

        //This Xpath identifies the worst performing Stocks Per CHG Percentage - Selects elements with Negative and Contains Percentage
        public By WorstPerformingStocksPerCHGpercentage()
        {
            By worst = By.XPath("//td[@class='table__cell w15']//bg-quote[@class='negative'][contains(text(), '%')]");
            return worst;
        }
    }
}
