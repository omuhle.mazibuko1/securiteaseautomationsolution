using SecuritEaseAssesment.Core;
using SecuritEaseAssesment.Functions;
using System;
using TechTalk.SpecFlow;

namespace SecuritEaseAssesment
{
    [Binding]
    public class ApiTestsStepDefinitions
    {
        ApiFunctions api = new ApiFunctions();

        [Given(@"I perform Get Request for All Countries")]
        public void GivenIPerformGetRequestForAllCountries()
        {
            api.doPerformAllCountriesGetRequest("","");
        }

        [When(@"I see response is displayed")]
        public void WhenISeeResponseIsDisplayed()
        {
            ApiCore.assertResponseNotEmpty();
        }

        [Then(@"I validate the response schema")]
        public void ThenIValidateTheResponseSchema()
        {
            ApiCore.verifyResponseSchema();
        }

        [Then(@"I locate the ""([^""]*)""")]
        public void ThenILocateThe(string country)
        {
            api.assertResponseContainsContry(country);
        }

        [Then(@"I assert the ""([^""]*)""")]
        public void ThenIAssertThe(string language)
        {
            api.assertResponseContainsLanguage(language);
        }

    }
}
