using SecuritEaseAssesment.Core;
using SecuritEaseAssesment.Functions;
using System;
using System.Web.Configuration;
using TechTalk.SpecFlow;

namespace SecuritEaseAssesment
{
    [Binding]
    public class WebTestsStepDefinitions
    {

        WebFunctions web = new WebFunctions();

        [Given(@"I open MarketWatch website")]
        public void GivenIOpenMarketWatchWebsite()
        {
          WebCore.initDriver();
        }

        [When(@"I go to MarketWatch tab")]
        public void WhenIGoToMarketWatchTab()
        {
            web.navigateMarketchTab();
        }

        [Then(@"I should see and retreive first seven articles and printout the name of each article")]
        public void ThenIShouldSeeAndRetreiveFirstSevenArticlesAndPrintoutTheNameOfEachArticle()
        {
            web.assertArticles();
        }

        [When(@"I list the top five worst performing stocks indexed by the change percentage \(CHG %\)")]
        [Obsolete]
        public void WhenIListTheTopFiveWorstPerformingStocksIndexedByTheChangePercentageCHG()
        {
            web.assertListOfTopFiveWorstPerformingStocks();
        }

        [Then(@"I print the names, last price \(LAST\) and the change percentage \(CHG %\)")]
        public void ThenIPrintTheNamesLastPriceLASTAndTheChangePercentageCHG()
        {
           
        }




    }
}
